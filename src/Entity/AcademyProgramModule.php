<?php

/**
 * @file
 * Contains \Drupal\academy\Entity\AcademyProgramModule
 */

namespace Drupal\academy\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the AcademyProgramModule entity type.
 *
 * This entity stores the relationship between a module and a program that
 * includes that module.
 *
 * @ContentEntityType(
 *   id = "academy_program_module",
 *   label = @Translation("ProgramModule"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage"
 *   },
 *   base_table = "academy_program_module",
 *   data_table = "academy_program_module_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *   }
 * )
 */
class AcademyProgramModule extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefiniation::create('integer')
      ->setLabel(t('ProgramModule ID'))
      ->setDescription(t('The program_module ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The program_module UUID.'))
      ->setReadOnly(TRUE);

    $fields['program'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Program'))
      ->setDescription(t('The program the module is taught on.'))
      ->setSetting('target_type', 'academy_program')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['module'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Module'))
      ->setDescription(t('The module being taught.'))
      ->setSetting('target_type', 'academy_module')
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['required'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Required'))
      ->setDescription(t('Whether this module must be studied for this program.'))
      ->setDefaultValue(TRUE);

    $fields['classes'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Classes'))
      ->setDescription(t('The classes that can be registered to.'))
      ->setSetting('target_type', 'academy_class')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    return $fields;
  }
}