<?php

/**
 * @file
 * Contains \Drupal\academy\Entity\AcademyModule
 */

namespace Drupal\academy\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the AcademyModule entity type.
 *
 * A module is a series of lessons about a given topic. Programs specify which
 * modules should be completed as part of their study. A class is formed to
 * represent a group of students studying a given module.
 *
 * @ContentEntityType(
 *   id = "academy_module",
 *   label = @Translation("Module"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage"
 *   },
 *   base_table = "academy_module",
 *   data_table = "academy_module_field_data",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *   }
 * )
 */
class AcademyModule extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefiniation::create('integer')
      ->setLabel(t('Module ID'))
      ->setDescription(t('The module ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The module UUID.'))
      ->setReadOnly(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The module language code.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'type' => 'hidden',
      ))
      ->setDisplayOptions('form', array(
        'type' => 'language_select',
        'weight' => 2,
      ));

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['code'] = BaseFieldDefinition::create('string')
     ->setLabel(t('Code'))
     ->setRequired(TRUE)
     ->setSetting('max_length', 20);

    $fields['description'] = BaseFieldDefinition::create('text_with_summary')
      ->setLabel(t('Description'))
      ->setTranslatable(TRUE);

    $fields['tutor'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tutor'))
      ->setDescription(t('The main tutor responsible for the module.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['assistants'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Assistants'))
      ->setDescription(t('Any staff that assist in the teaching of this module.'))
      ->setSetting('target_type', 'user')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    // Todo session templates.

    return $fields;
  }
}