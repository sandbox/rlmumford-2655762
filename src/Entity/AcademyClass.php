<?php

/**
 * @file
 * Contains \Drupal\academy\Entity\AcademyClass
 */

namespace Drupal\academy\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the AcademyClass entity type.
 *
 * A class is a group of people undertaking a series of sessions together to
 * be taught the content of a module.
 *
 * @ContentEntityType(
 *   id = "academy_class",
 *   label = @Translation("Class"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage"
 *   },
 *   base_table = "academy_class",
 *   data_table = "academy_class_field_data",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *   }
 * )
 */
class AcademyClass extends ContentEntityBase {

  /**
   * The pending status string.
   *
   * A class is pending if it has not started yet and is not yet open to new
   * registrations.
   *
   * @var string
   */
  public const STATUS_PENDING = 'pending';

  /**
   * The open status string.
   *
   * A class is open if it has not started yet and is open to new
   * registrations.
   *
   * @var string
   */
  public const STATUS_OPEN = 'open';

  /**
   * The closed status string.
   *
   * A class is closed if it has not started yet but has finished accepting new
   * registrations.
   *
   * @var string
   */
  public const STATUS_CLOSED = 'closed';

  /**
   * The in progress status string.
   *
   * A class is in progess if it has started but not finished, in progress
   * classes are usually closed to new registrations.
   *
   * @var string
   */
  public const STATUS_IN_PROGRESS = 'in_progress';

  /**
   * The finished status string.
   *
   * A class is finished if all sessions have been taught.
   *
   * @var string
   */
  public const STATUS_FINISHED = 'finished';

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefiniation::create('integer')
      ->setLabel(t('Class ID'))
      ->setDescription(t('The class ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The class UUID.'))
      ->setReadOnly(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The class language code.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'type' => 'hidden',
      ))
      ->setDisplayOptions('form', array(
        'type' => 'language_select',
        'weight' => 2,
      ));

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['module'] = BaseFieldDefinition::create('entity_reference')
     ->setLabel(t('Module'))
     ->setSetting('target_type', 'academy_module')
     ->setRequired(TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['start'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Start Date'))
      ->setDescription(t('The date the class starts on.'))
      ->setRequired(TRUE)
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE);

    $fields['description'] = BaseFieldDefinition::create('text_with_summary')
      ->setLabel(t('Description'))
      ->setTranslatable(TRUE);

    $fields['tutor'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tutor'))
      ->setDescription(t('The main tutor responsible for the class.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['assistants'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Assistants'))
      ->setDescription(t('Any staff that assist in the teaching of this class.'))
      ->setSetting('target_type', 'user')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of this class.'))
      ->setSetting('allowed_values', [
        self::STATUS_PENDING => t('Pending'),
        self::STATUS_OPEN => t('Open'),
        self::STATUS_CLOSED => t('Closed'),
        self::STATUS_IN_PROGRESS => t('In Progress'),
        self::STATUS_FINISHED => t('Finished'),
      ]);

    // Todo session templates.

    return $fields;
  }
}