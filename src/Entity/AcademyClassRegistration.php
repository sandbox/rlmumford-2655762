<?php

/**
 * @file
 * Contains \Drupal\academy\Entity\AcademyClassRegistration
 */

namespace Drupal\academy\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the AcademyClassRegistration entity type.
 *
 * A class registration stores the relationship between a student and a class.
 *
 * @ContentEntityType(
 *   id = "academy_class_registration",
 *   label = @Translation("Class Registration"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage"
 *   },
 *   base_table = "academy_class_registration",
 *   data_table = "academy_class_registration_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *   }
 * )
 */
class AcademyClassRegistration extends ContentEntityBase {

  /**
   * The active status string.
   *
   * A registration is active if the student is still studying the class.
   *
   * @var string
   */
  public const STATUS_ACTIVE = 'active';

  /**
   * The cancelled status string.
   *
   * @var string
   */
  public const STATUS_CANCELLED = 'cancelled';

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefiniation::create('integer')
      ->setLabel(t('ClassRegistration ID'))
      ->setDescription(t('The class_registration ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The class_registration UUID.'))
      ->setReadOnly(TRUE);

    $fields['student'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Student'))
      ->setDescription(t('The student registered to the class.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['class'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Class'))
      ->setDescription(t('The class the student is registered to.'))
      ->setSetting('target_type', 'academy_class')
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['program'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Program'))
      ->setDescription(t('The program the student is registered to.'))
      ->setSetting('target_type', 'academy_program')
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of this registration.'))
      ->setSetting('allowed_values', [
        self::STATUS_ACTIVE => t('Active'),
        self::STATUS_CANCELLED => t('Cancelled'),
      ]);

    return $fields;
  }
}